/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miniprojetserveur;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/**
 *
 * @author arthurdambrine
 */
public class IOCommandes {

    private final BufferedReader lectureEcran;
    private final PrintStream ecritureEcran;

    private final BufferedReader lectureReseau;
    private final PrintStream ecritureReseau;

    // private final BufferedReader lectureFichier;
    // private final PrintStream ecritureFichier;
    // Constructeur
    public IOCommandes(Socket socket) throws IOException {
        lectureEcran = new BufferedReader(new InputStreamReader(System.in));
        ecritureEcran = System.out; // = System.out équivalent à new PrintStream(System.out)

        lectureReseau = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        ecritureReseau = new PrintStream(socket.getOutputStream());

    }

    // Constructeur
    public IOCommandes(Socket socket, File monfichier) throws IOException {
        lectureEcran = new BufferedReader(new InputStreamReader(System.in));
        ecritureEcran = System.out; // = System.out équivalent à new PrintStream(System.out)

        lectureReseau = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        ecritureReseau = new PrintStream(socket.getOutputStream());

        //  lectureFichier = new BufferedReader(new FileReader(monfichier));
        //  ecritureFichier = null;
    }

    // Méthodes 
    public void ecrireEcran(String texte) {
        ecritureEcran.println(texte);
    }

    public String lireEcran() throws IOException {
        return this.lectureEcran.readLine();
    }

    public void ecrireReseau(String texte) {
        ecritureReseau.println(texte);
    }

    public String lireReseau() throws IOException {
        return this.lectureReseau.readLine();
    }

    public String lireFichier() throws IOException {
        //    return this.lectureFichier.readLine();
        return "";
    }
}
