/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miniprojetserveur;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author arthurdambrine
 */
public class MiniProjetClientChat {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
//        Socket mySocket = new Socket("192.168.1.140", 5999);
        Socket mySocket = new Socket("localhost", 6060);
//        Socket mySocket = new Socket("192.168.1.197", 8888);
        // File monFichier = new File("/Users/arthurdambrine/Desktop/test/monfichier");
        IOCommandes io = new IOCommandes(mySocket/*,monFichier*/);
//        io.ecrireEcran("hello");
        String s = "";
//        do {
//            s = io.lireEcran();
//            io.ecrireEcran(s);
//        } while (s.equals("quit") == false);

//        io.ecrireReseau("Coucou c'est encore nous !");
//        io.ecrireEcran(io.lireReseau());

        System.out.println("Bienvenue sur le chat :\n Liste des commandes :\n"
                + "  - '' /all <votre message> '' permet de communiquer avec l’ensemble des utilisateurs connectés.\n"
                + "  - '' /quit '' vous permet de vous deconnecter.\n");

// ====  Boucle client vers serveur =====
        int i = 0;
        s = io.lireReseau();
        io.ecrireEcran(s);     

        if (s.equals("Nombre de clients max atteints") == true) {
            s = "/quit";
        }
        String s2;
        s2 = io.lireReseau(); //Demande à l'utilisateur de se connecter
        io.ecrireEcran(s2);

        s2 = io.lireEcran();//Lis le pseudo entré par l'utilisateur
        io.ecrireReseau(s2); //Envoi le pseudo au serveur

        //Verification du message envoyé par le serveur
        s = io.lireReseau();
        io.ecrireEcran(s);
        
        

        if (s.equals("Connexion ok")) {

            LecteurReseauThread lecteur = new LecteurReseauThread(mySocket);
            
            while (s.equals("/quit") == false) {
                s = io.lireEcran();
                io.ecrireReseau(s);
            }
            lecteur.interrupt();
        }

// ======= Lecture fichier =======
//        do {
//            s = io.lireFichier();
//            if (s != null) {
//                io.ecrireEcran(s);
//            }
//        } while (s != null);
    }

}
