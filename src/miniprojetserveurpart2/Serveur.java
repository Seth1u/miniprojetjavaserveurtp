/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miniprojetserveurpart2;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Vector;

public class Serveur {

    public static void main(String[] zero) throws IOException {

        ServerSocket socketserver;
        Socket socketduserveur = null;
        Thread t;
        int count = 1;
        int nbConnexionsAutorisees = 4;
        Vector users = new Vector();

        ArrayList<RunImplement> arrayRun = new ArrayList();

        socketserver = new ServerSocket(6060);
        int i = 0;
        boolean continuer = true;
        while (continuer) {

            socketduserveur = socketserver.accept();
            t = new Thread(new RunImplement(socketduserveur, count, nbConnexionsAutorisees, users, arrayRun));
            t.start();
            count = Thread.activeCount();

            if (count < nbConnexionsAutorisees) {
                System.out.println("Actuellement : " + (count - 1) + " clients connectés");
            } else {
                System.out.println("Dépassement du nombre de connexions autorisées");
            }
            // System.out.println(users.firstElement());
        }

        System.out.println("Fermeture serveur!");
        socketserver.close();
        socketduserveur.close();

    }

}
