/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miniprojetserveurpart2;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import miniprojetserveur.IOCommandes;

/**
 *
 * @author arthurdambrine
 */
public class RunImplement implements Runnable {

    private Socket socket;
    private int count;
    private int coAutorisees;
    private Vector usersList;
    private ArrayList<RunImplement> arrayRun;
    private IOCommandes io;

    public RunImplement(Socket socket, int count, int coAutorisees, Vector users, ArrayList<RunImplement> arrayRun) {

        this.socket = socket;
        this.count = count;
        this.coAutorisees = coAutorisees;
        this.usersList = users;
        this.arrayRun = arrayRun;
    }

    public void write(String s) {
        for (RunImplement mRunnable : arrayRun) {
            mRunnable.io.ecrireReseau(s);
        }

    }

    @Override
    public void run() {
        try {

            io = new IOCommandes(this.socket);
            String s;
            if (count <= coAutorisees) {
                s = "(Actuellement) Nombre de clients sur le serveur " + count;
            } else {
                s = "Nombre de clients max atteints";
            }
            io.ecrireReseau(s);

            String s2;
            s2 = "Connectez vous :";
            io.ecrireReseau(s2);

            String pseudo;
            pseudo = io.lireReseau();

            if (!usersList.contains(pseudo)) {
                s = "Connexion ok";
                io.ecrireReseau(s);
                usersList.add(pseudo);

                arrayRun.add(this);

                if (count <= coAutorisees) { // si déjà plus de 4 clients connectés on ferme la socket

                    while (s.equals("/quit") == false) {
                        s = io.lireReseau();
                        if (s.equals("null") == false) {
                            io.ecrireEcran(s);
                            String echo;
                            if (s.contains("/all")) {
                                echo = socket.getInetAddress() + " echo> " + s;
                                s = s.replace("/all", "");
                                write("("+pseudo+")"+s);
                            } else {
                                echo = socket.getInetAddress() + " echo> " + s;
                            }
                            io.ecrireReseau(echo);
                        }
                    }
                }

                io.ecrireReseau("/quit");
                usersList.remove(pseudo);
            } else {
                s = "/quit";
                io.ecrireReseau(s);
            }

            this.socket.close();

        } catch (IOException ex) {
            Logger.getLogger(RunImplement.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
